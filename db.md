|URL|Podcast Description|Site Description
|---|----------|--------|
|https://github.com/sachaos/viddy/blob/master/README.md|viddy|Coming soon maybe|
|https://github.com/ralight/cheerlights-hid|cheerlights-hid|Coming soon maybe|
|https://github.com/bkerler/netgear_telnet|Hacking a Netgear router to be a ‘mesh’ satellite|Coming soon maybe|
|https://github.com/Vita3K/Vita3K|Vita3K|Coming soon maybe|
|https://www.neowin.net/news/rufus-319-adds-bypass-for-mandatory-windows-11-22h2-microsoft-account-requirement/|Rufus 3.19 adds bypass for mandatory Windows 11 22H2 Microsoft Account requirement|Coming soon maybe|
|https://www.tomshardware.com/news/raspberry-pi-guitar-amp|Raspberry Pi Restores Guitar Amp, Complete With Effects|Coming soon maybe|
|https://github.com/libratbag/piper/|piper|Coming soon maybe|
|https://www.weewx.com/|WeeWX|Coming soon maybe|
|https://github.com/webosbrew/dev-manager-desktop|Device/DevMode Manager for webOS TV|Coming soon maybe|
|https://youtu.be/h5UmPTttN1s|LMN 3: An Open-Source DAW-in-a-Box|Coming soon maybe|
|https://next-hack.com/index.php/2021/11/13/porting-doom-to-an-nrf52840-based-usb-bluetooth-le-dongle/|on a Bluetooth dongle|Coming soon maybe|
|https://obsproject.com/|OBS|Coming soon maybe|
|https://wazuh.com/|Wazuh|Coming soon maybe|
|https://github.com/cheat/cheat|cheat|Coming soon maybe|
|https://bigdanzblog.wordpress.com/2022/05/23/cheap-logic-analyzer-examined/|here|Coming soon maybe|
|https://github.com/rvaiya/warpd|warpd|Coming soon maybe|
|https://wiki.archlinux.org/title/archinstall|archinstall|Coming soon maybe|
|https://www.dwitter.net/|Dwitter|Coming soon maybe|
|https://huggingface.co/spaces/dalle-mini/dalle-mini|DALL·E mini|Coming soon maybe|
|https://rustdesk.com/|RustDesk|Coming soon maybe|
|https://flows.nodered.org/node/node-red-contrib-virtual-smart-home|Virtual Smart Home|Coming soon maybe|
|https://jellyfin.org|Jellyfin|Coming soon maybe|
|https://blog.thunderbird.net/2022/05/thunderbird-rss-feeds-guide-favorite-content-to-the-inbox/|it has an RSS reader|Coming soon maybe|
|https://surge-synthesizer.github.io/|Surge XT synth|Coming soon maybe|
|https://www.theregister.com/2022/05/04/unity_trinity_desktops/|as is Trinity|Coming soon maybe|
|https://www.youtube.com/watch?v=eJT8xuI_5PY|10 hours of a hairdryer noise|Coming soon maybe|
|https://ubuntuincident.wordpress.com/2012/01/06/playing-star-trek-background-noise-with-a-linux-command/|Star Trek TNG bridge noise|Coming soon maybe|
|https://github.com/CZ-NIC/pz|pz|Coming soon maybe|
|https://dashy.to/|dashy|Coming soon maybe|
|https://twitter.com/zorinaq/status/1176625574762053632|Charge your laptop off a big external battery over USB-C|Coming soon maybe|
|https://github.com/ct-Open-Source/tuya-convert|Tuya Convert|Coming soon maybe|
|https://github.com/eradman/entr|entr|Coming soon maybe|
|https://hackaday.com/2022/04/25/insteon-abruptly-shuts-down-users-left-smart-home-less/|Insteon Abruptly Shuts Down, Users Left Smart-Home-Less|Coming soon maybe|
|https://github.com/KSXGitHub/parallel-disk-usage|Parallel Disk Usage|Coming soon maybe|
|https://github.com/jpochyla/psst|psst|Coming soon maybe|
|https://debconf22.debconf.org/news/2022-04-14-touristic-guide/|Touristic Guide — DebConf 22|Coming soon maybe|
|https://pypi.org/project/adblock/|adblock · PyPI|Coming soon maybe|
|https://matrix.to/#/%23LateNightLinux:matrix.org|LNL Matrix|Coming soon maybe|
|https://element.io/|Element|Coming soon maybe|
|https://apps.apple.com/us/app/mastodon-for-iphone/id1571998974|iOS|Coming soon maybe|
|https://rss.app/rss-feed/create-twitter-rss-feed|Create Twitter RSS Feeds|Coming soon maybe|
|https://github.com/warpnet/salt-lint/|Saltstack linter|Coming soon maybe|
|https://github.com/Wilfred/difftastic|difftastic|Coming soon maybe|
|https://github.com/xournalpp/xournalpp|Xournalpp|Coming soon maybe|
|https://github.com/denisidoro/navi|navi|Coming soon maybe|
|https://codeberg.org/Okxa/qddcswitch|qddcswitch|Coming soon maybe|
|https://github.com/popey/unsnap|unsnap|Coming soon maybe|
|https://asciinema.org/|asciinema|Coming soon maybe|
|https://wizardzines.com/|Julia Evans|Coming soon maybe|
|https://www.influxdata.com/time-series-platform/telegraf/|Telegraf|Coming soon maybe|
|https://github.com/jesseduffield/horcrux|Horcrux|Coming soon maybe|
|https://github.com/libimobiledevice/idevicerestore|a FOSS alternative|Coming soon maybe|
|https://www.unix.com/man-page/linux/1/aha/|aha|Coming soon maybe|
|https://thewalrus.ca/how-canada-accidentally-helped-crack-computer-translation/|blame Canada for computer translation|Coming soon maybe|
|https://chirp.danplanet.com/projects/chirp/wiki/Running_Under_Linux|Chirp|Coming soon maybe|
|https://github.com/vimwiki/vimwiki|vimwiki|Coming soon maybe|
|https://www.youtube.com/watch?v=9XMfKYVu_fg|video|Coming soon maybe|
|https://github.com/Lyptik/Borderlands|Borderlands|Coming soon maybe|
|https://pandas.pydata.org/pandas-docs/stable/user_guide/10min.html|10mins to pandas|Coming soon maybe|
|https://kno.wled.ge/|WLED|Coming soon maybe|
|https://kernc.github.io/xsuspender|XSuspender|Coming soon maybe|
|https://www.uni-due.de/~be0001/subnetcalc/|subnetcalc|Coming soon maybe|
|https://github.com/quickemu-project/quickgui|Quickgui|Coming soon maybe|
|https://github.com/Textualize/rich|Python Rich|Coming soon maybe|
|https://www.sweethome3d.com/|Sweethome 3D|Coming soon maybe|
|https://github.com/cwackerfuss/react-wordle|react-wordle|Coming soon maybe|
|https://gist.github.com/huytd/6a1a6a7b34a0d0abcac00b47e3d01513|wordle in under 50 lines of bash|Coming soon maybe|
|https://github.com/Bismuth-Forge/bismuth|Bismuth|Coming soon maybe|
|https://github.com/esjeon/krohnkite|Kröhnkite|Coming soon maybe|
|https://github.com/kwin-scripts/kwin-tiling|KWin-Tiling|Coming soon maybe|
|https://github.com/buddhi1980/mandelbulber2|Mandelbulber2|Coming soon maybe|
|https://stedolan.github.io/jq/|Jq|Coming soon maybe|
|https://vital.audio/|Vital synth|Coming soon maybe|
|https://www.hifiberry.com/hifiberryos/|HiFi Berry OS|Coming soon maybe|
|https://www.nathanmediaservices.co.uk/teletext-viewer/|Ceefax lives!|Coming soon maybe|
|https://github.com/h2zero/NimBLE-Arduino|NimBLE ESP32|Coming soon maybe|
|https://www.theregister.com/2022/01/17/bbc_iplayer_expires_2038/|iPlayer probably runs on 32-bit Linux|Coming soon maybe|
|https://github.com/debauchee/barrier|Barrier|Coming soon maybe|
|https://github.com/pauldreik/rdfind|rdfind|Coming soon maybe|
|https://github.com/katef/eurorack-cpu|A CPU implemented in a modular synthesizer|Coming soon maybe|
|https://manpages.debian.org/testing/moreutils/ts.1.en.html|ts|Coming soon maybe|
|https://codewithrockstar.com/docs|The Rockstar Language Specification|Coming soon maybe|
