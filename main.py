#!/usr/bin/python

import re
import requests

url = 'https://latenightlinux.com/feed/mp3'

r = requests.get(url)
fr = open('feed.rss','w')
fr.write(r.content.decode())
fr.close()

fr = open('feed.rss')
fw = open('db.md','w')

val = False
linkArray = [[],[]]

for x in fr:
    if '<p><strong>Discoveries' in x:
        x = x.strip()
        val = True
        continue
    if (val == True):
        x = x.strip()
        if ('<a href="' in x):
            #URL
            url = re.sub(r'^.*="', '', x)
            url = re.sub(r'".*$','',url)
            linkArray[0].append(url)
            #Desciption
            description = re.sub(r'^.*">','',x)
            description = re.sub(r'<.*$','',description)
            linkArray[1].append(description)
        if '<p><strong>' in x:
            val = False

print(("Found %i URLs with their descriptions")%len(linkArray[0]))

fw.write('|URL|Podcast Description|Site Description\n|---|----------|--------|\n')
for x in range(0,len(linkArray[1])):
    fw.write('|')
    fw.write(linkArray[0][x])
    fw.write('|')
    fw.write(linkArray[1][x])
    fw.write('|')
    fw.write('Coming soon maybe|\n')
